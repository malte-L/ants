package main


import "../kdtree"
import "core:fmt"
import "core:math/rand"
import "core:time"

import rl "vendor:raylib"


MyValues :: struct {}


main :: proc() {
	tree := kdtree.empty(MyValues)
	defer kdtree.free_tree(tree)
	assert(tree != nil)


	for i in 0 ..= 100 {
		next := kdtree.Position{rand.float32() * 20, rand.float32() * 20}
		fmt.println(i, " inserting: ", next)
		kdtree.insert(tree, MyValues{}, next)
	}

	time.sleep(time.Second * 2)


	for i in 0 ..= 100 {
		next := kdtree.Position{rand.float32() * 20, rand.float32() * 20}
		fmt.println(i, " finding_nearest: ", next)
		kdtree.find_nearest(tree^, next)
	}


	fmt.println("hello world")


	rl.InitWindow(100, 100, "dot")
	rl.SetWindowState(rl.ConfigFlags{.WINDOW_RESIZABLE})
	rl.SetTargetFPS(60)


	for !rl.WindowShouldClose() {

		rl.BeginDrawing()
		{

		}
		rl.EndDrawing()
	}


}
