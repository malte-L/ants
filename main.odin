package main

import "core:fmt"
import "core:os"
import "core:c"
import "core:mem"
import "core:math"
import "core:time"
import "core:math/rand"
import "core:strings"
import "core:strconv"
import rl "vendor:raylib"

import "./kdtree"




// config

PHEROMONE_HEAP_SIZE :: mem.Megabyte 
MAX_PHEROMONES: int = PHEROMONE_HEAP_SIZE

ANTS_HEAP_SIZE :: 1000
NUM_ANTS : int = 100


NUM_BASES :: 3
ANT_SPEED :: f32(3)
PHEROME_SPEED :: 10
NUM_FOOD_CLUSTERS :: 10


Window :: struct {
	name:          cstring,
	width:         i32,
	height:        i32,
	fps:           i32,
	control_flags: rl.ConfigFlags,
}


generation: int

V2 :: [2]f32

Entity :: struct {
	pos: V2,
}

Ant :: struct {
	using entity: Entity,
	direction:    V2,
	target: Pheromone_Type,
}

Food :: struct {
	using entity: Entity,
}


Pheromone :: struct {
	pos: V2,
	_type: Pheromone_Type, 
}


Pheromone_Type :: enum u8 {
	Food,
	Base,
}

pheromone_trail_type_from_target_type := [Pheromone_Type]Pheromone_Type {
	.Base = .Food,
	.Food = .Base,
}

Pheromone_Type_Tree_Mapping :: [Pheromone_Type]^kdtree.Tree(struct{})


pheromone_heap: struct {
	mem: [PHEROMONE_HEAP_SIZE]Pheromone,
	offset: int,
}

ants: [ANTS_HEAP_SIZE]Ant


//Pheromone_Ptr :: int


new_pheromone :: proc(pos: V2, _type: Pheromone_Type) {
	using pheromone_heap
	defer offset = (offset + 1) % MAX_PHEROMONES
	mem[offset]._type = _type
	mem[offset].pos = pos
	//return offset
}




ANT_LENGTH :: f32(10)


ant_base_counters: [dynamic]int



draw_ant :: proc(using ant: Ant) {
	head := pos + direction * ANT_LENGTH
	rl.DrawLineV(rl.Vector2(pos), rl.Vector2(head), rl.WHITE)


	@static 
	colors: [Pheromone_Type]rl.Color
	colors = [Pheromone_Type]rl.Color{
		.Base = rl.RED,
		.Food = rl.BLACK,
	}
	rl.DrawCircleV(rl.Vector2(head), 4, colors[target])
}

update_ant :: proc(using ant: ^Ant, food_sources: ^kdtree.Tree(Food), mapping: Pheromone_Type_Tree_Mapping, ant_bases: [dynamic]V2) {
	if generation % PHEROME_SPEED == 0 {
		/*
		{	
			// this does nothing ??
			using pheromone_heap
			for p_type in Pheromone_Type do kdtree.remove(mapping[p_type], mem[offset].pos)
		}
		*/

		new_pheromone(kdtree.Position(pos), pheromone_trail_type_from_target_type[target]) 
		kdtree.insert(mapping[pheromone_trail_type_from_target_type[target]], struct{}{}, kdtree.Position(pos))
	}

	head := pos + direction  * ANT_LENGTH



	tree := mapping[target]




	bucket := kdtree.find_containing_bucket(tree^, kdtree.Position(head))
	new_direction: V2
	x_count: f32
	for x in bucket.mem[:bucket.count] {
		if _distance(V2(x.pos), head) < 300 && dot(head - pos, V2(x.pos) - pos) > 0 {
			new_direction += normalize(V2(x.pos) - pos)
			x_count += 1
			//kdtree.remove(tree, x.pos) // bad results otherwise
		}
	}
	if x_count > 0 {
		new_direction /= x_count
		direction = new_direction
	} else {
		direction = direction + ({rand.float32(), rand.float32()} - {.5, .5}) / 8
	}


	/*
	// somehow works
	if  nearest, found_nearest := kdtree.find_nearest(tree^, kdtree.Position(head));
	     		found_nearest && _distance(V2(nearest.pos), head) < 300 && 

	     		dot(head - pos, V2(nearest.pos) - pos) > 0{
			direction = normalize(V2(nearest.pos) - pos) // something is slightly off
			kdtree.remove(tree, kdtree.Position(nearest.pos))
	} else {
		direction = direction + ({rand.float32(), rand.float32()} - {.5, .5}) / 8
	}
	*/ 
	next_pos := pos + direction * ANT_SPEED


	if next_pos.x < 0 || next_pos.x > f32(window.width) do direction *= -rand.float32()
	if next_pos.y < 0 || next_pos.y > f32(window.height) do direction *= -rand.float32()
	pos = pos + direction * ANT_SPEED



	switch target {
	case .Food:
		if nearest_food, found_nearest_food := kdtree.find_nearest(food_sources^, pos); found_nearest_food && _distance(nearest_food.pos, pos) < 20 {
			kdtree.remove(food_sources, nearest_food.pos)
			direction *= -1
			target = .Base
		}
	case .Base: 
		// todo: use kdtree for bases as well?
		for ant_base, i in ant_bases {
			if _distance(ant_base, pos) < 20 {
				direction = pos - head
				target = .Food
				ant_base_counters[i]+=1
			}
		}
	}



	//
	normalize :: proc(v: V2) -> V2 {
		length := math.sqrt(v.x * v.x + v.y * v.y)
		if length == 0 {
			return v
		}
		return v / length
	}

	dot :: proc(v1, v2: V2) -> f32 {
		t := v1 * v2
		return t.x + t.y
	}


	_distance :: proc(d1, d2: [2]f32) -> f32 {
		return math.sqrt((d1.x - d2.x) * (d1.x - d2.x) + (d1.y - d2.y) * (d1.y - d2.y))
	}
}


generate_food_cluster :: proc(center: V2, radius: f32, store: ^kdtree.Tree(Food), pheromone_store: ^kdtree.Tree(struct{})) {
	MAX_FOOD_CLUSTER_SIZE :: i64(400)

	amount := MAX_FOOD_CLUSTER_SIZE
	for i in 0 ..< amount {
		theta := rand.float32() * 2 * math.PI
		random_radius := rand.float32() * radius
		offset := V2{math.cos(theta), math.sin(theta)} * random_radius
		food := Food {
			entity = Entity{center + offset},
		}
		kdtree.insert(store, food, center + offset)
		kdtree.insert(pheromone_store, struct{}{}, center + offset)
	}
}



draw_tree :: proc(tree: kdtree.Tree($T), draw_dot: proc(kdtree.Dot(T)), show_kddivisions := false) {
	kdtree.traverse(tree, rawptr(draw_dot), sproc, bproc)

	sproc :: proc(s: kdtree.Subdivision(T), data: rawptr) {}

	bproc :: proc(using b: ^kdtree.Bucket(T), data: rawptr) {
		draw_dot := cast(proc(kdtree.Dot(T)))data
		for i in 0 ..< count do draw_dot(mem[i])
	}
}



window := Window{"Game Of Life", 1200, 800, 120, rl.ConfigFlags{.WINDOW_RESIZABLE}}


main :: proc() {
	read_config_file()

	ant_bases: [dynamic]V2

	ant_base_counters = make([dynamic]int, NUM_BASES)
	for i in 0..<NUM_BASES {
		append(&ant_bases, V2{rand.float32(), rand.float32()} * {f32(window.width), f32(window.height)})
	}




	pheromones: Pheromone_Type_Tree_Mapping
	for t in Pheromone_Type do pheromones[t] = kdtree.empty(struct{}) 


	last_frame_pheromone_offset: int
	food_sources := kdtree.empty(Food)


	for i in 0..<NUM_FOOD_CLUSTERS {
		generate_food_cluster({rand.float32(), rand.float32()} * {f32(window.width), f32(window.height)},  f32(rand.int63() % 100), food_sources, pheromones[.Food])
	} 


	for base in ant_bases {
		step := f32(math.PI * 2 / 100)
		for theta := f32(0); theta < math.PI*2; theta+=step {
			p := base +  {math.cos(theta), math.sin(theta)}
			kdtree.insert(pheromones[.Base], struct{}{} , kdtree.Position(p * 60))
		}
	}


	with_alpha :: proc(c: rl.Color, a: u8) -> rl.Color {
		cc := c
		cc.a = a
		return cc
	}

	color_map := [Pheromone_Type]rl.Color{
		.Base =   with_alpha(rl.PURPLE, 100),
		.Food =   with_alpha(rl.RED, 100),
	}


	rl.InitWindow(window.width, window.height, window.name)
	rl.SetWindowState(window.control_flags)
	rl.SetTargetFPS(window.fps)


	for !rl.WindowShouldClose() {
		read_config_file()




		@static 
		last_num_ants: int
		if NUM_ANTS > last_num_ants {
			fmt.println("spawning", NUM_ANTS-last_num_ants, "new ants")
			for i in last_num_ants..<NUM_ANTS {
				base := i % NUM_BASES
				ants[i] = Ant {
					entity = Entity{ant_bases[base]},
					direction = {rand.float32(), rand.float32()}
				}
			}			
		}
		last_num_ants = NUM_ANTS

		@static 
		last_max_pheromones: int
		if MAX_PHEROMONES != last_max_pheromones {
			fmt.println("freeing pheromone trees")
			for t in Pheromone_Type {
				kdtree.free_tree(pheromones[t])
				pheromones[t] = kdtree.empty(struct{})
			}
			for i in 0..<min(last_max_pheromones, MAX_PHEROMONES) {
				p := pheromone_heap.mem[i]
				kdtree.insert(pheromones[p._type], struct{}{}, kdtree.Position(p.pos))
			}
		}
		last_max_pheromones = MAX_PHEROMONES



		generation += 1

		window.width = i32(rl.GetScreenWidth())
		window.height = i32(rl.GetScreenHeight())


		rl.BeginDrawing()
		{
			rl.ClearBackground(rl.BLACK)


			draw_tree(food_sources^, proc(using food: kdtree.Dot(Food)) {
				rl.DrawCircleV(rl.Vector2(pos), 2, rl.RED)
			})



			for i in 0..<MAX_PHEROMONES do rl.DrawPixelV(rl.Vector2(pheromone_heap.mem[i].pos), color_map[pheromone_heap.mem[i]._type])

			for i in 0..<NUM_ANTS {
				update_ant(&ants[i], food_sources, pheromones, ant_bases)
				draw_ant(ants[i])
			}


			for ant_base, i in ant_bases {
				rl.DrawCircleV(rl.Vector2(ant_base), 20, rl.PURPLE)
				rl.DrawText(strings.unsafe_string_to_cstring(fmt.tprint(ant_base_counters[i])), i32(ant_base[0]), i32(ant_base[1]), 10, rl.WHITE)
			}

			draw_config()
		}
		rl.EndDrawing()
	}


}



read_config_file :: proc() {
	file_info, errno := os.stat("config")
	if errno != os.ERROR_NONE do return

	@static 
	last_modification_time: time.Time
	if file_info.modification_time == last_modification_time do return
	last_modification_time = file_info.modification_time

	fmt.println("reading config file..")

	file_data, success := os.read_entire_file("config")
	if !success do return 
	defer delete(file_data)



	iterator := string(file_data)
	for line in strings.split_lines_iterator(&iterator) {
		fields := strings.fields(line)
		if len(fields) != 3 do continue
		switch fields[0] {
		case "MAX_PHEROMONES": 
			x, ok :=strconv.parse_int(fields[2])
			if !ok do break
			MAX_PHEROMONES = math.min(x, PHEROMONE_HEAP_SIZE)

		case "NUM_ANTS":
			x, ok :=strconv.parse_int(fields[2])
			if !ok do break
			NUM_ANTS = math.min(x, ANTS_HEAP_SIZE)


		}



	}
}


draw_config :: proc() {
	size :: c.int(14)

	rl.DrawText(strings.unsafe_string_to_cstring(fmt.tprint("MAX_PHEROMONES: ", MAX_PHEROMONES)), 0, 0, size, rl.RAYWHITE)
	rl.DrawText(strings.unsafe_string_to_cstring(fmt.tprint("NUM_ANTS: ", NUM_ANTS)), 0, 14, size, rl.RAYWHITE)


}
