package kdtree

import "core:fmt"
import "core:math"


BUCKET_SIZE :: 80

Bucket :: struct(T: typeid) {
	mem:   [BUCKET_SIZE]Dot(T),
	count: int,
}


// Dir indicates how a tree will be split once it overflows
// note: the order of these matters!: 
// 		when split vertically, we compare the x (index = 0) components of the entries
// 		when split horizontally, we compare y (index = 1)
Dir :: enum u8 {
	Vertical,
	Horizontal,
}

Dot :: struct(T: typeid) {
	value: T,
	pos:   Position,
}

Position :: [2]f32

Tree :: struct(T: typeid) {
	dir:   Dir,
	value: union {
		^Bucket(T),
		Subdivision(T),
	},
}

Subdivision :: struct(T: typeid) {
	at:    f32,
	sides: [2]^Tree(T),
}


empty :: proc($T: typeid) -> ^Tree(T) {
	ret := new_clone(Tree(T){})
	ret.value = new_clone(Bucket(T){})
	return ret
}

free_tree :: proc(tree: ^Tree($T)) {
	switch value in tree.value {
	case Subdivision(T):
		free_tree(value.sides[0])
		free_tree(value.sides[1])
	case ^Bucket(T):
		free(value)
	}

	free(tree)
}

insert :: proc(tree: ^Tree($T), value_to_insert: T, pos: Position) {
	@(static)
	called_count: int
	called_count += 1

	switch value in tree.value {
	case Subdivision(T):
		using value
		if pos[tree.dir] < at {
			insert(sides[0], value_to_insert, pos)
		} else {
			insert(sides[1], value_to_insert, pos)
		}
	case ^Bucket(T):
		bucket := value
		if bucket.count + 1 < BUCKET_SIZE {
			bucket.mem[bucket.count] = Dot(T){value_to_insert, pos}
			bucket.count += 1
		} else {
			//fmt.println("kdtree.insert: split!", called_count)
			median: f32
			for i in 0 ..< bucket.count do median += bucket.mem[i].pos[tree.dir]
			median /= f32(bucket.count)


			subdivision := Subdivision(T) {
				at = median,
				sides = [2]^Tree(T){empty(T), empty(T)},
			}
			subdivision.sides[0].dir = .Vertical if tree.dir == .Horizontal else .Horizontal
			subdivision.sides[1].dir = .Vertical if tree.dir == .Horizontal else .Horizontal
			tree.value = subdivision

			for i in 0 ..< bucket.count {
				if bucket.mem[i].pos[tree.dir] < median {
					insert(subdivision.sides[0], bucket.mem[i].value, bucket.mem[i].pos)
				} else {
					insert(subdivision.sides[1], bucket.mem[i].value, bucket.mem[i].pos)
				}
			}

			free(bucket)
		}
	}
}


remove_by_position :: proc(tree: ^Tree($T, $N), pos: Position) -> bool {
	switch value in tree.value {
	case Subdivision(T):
		if pos[tree.dir] < value.at {
			return remove(value.sides[0], pos)
		} else {
			return remove(value.sides[1], pos)
		}
	case ^Bucket(T):
		for i in 0 ..< value.count {
			if value.mem[i].pos == pos {
				value.mem[i] = value.mem[value.count]
				value.count -= 1
				return true
			}
		}
	}

	return false
}


remove :: proc {
	remove_by_position,
}


find_nearest :: proc(
	using tree: Tree($T, $N),
	pos: Position,
	loc := #caller_location,
) -> (
	Dot(T),
	bool,
) {
	switch value in tree.value {
	case Subdivision(T):
		//fmt.println("kdtree.find_nearest: Subdivision")

		subdivision := value
		if pos[dir] < subdivision.at {
			return find_nearest(subdivision.sides[0]^, pos)
		} else {
			return find_nearest(subdivision.sides[1]^, pos)
		}
	case ^Bucket(T):
		//fmt.println("kdtree.find_nearest: ^Bucket")
		found: Dot(T)
		max_distance_so_far := f32(100000)
		for i in 0 ..< value.count {
			d := _distance(pos, value.mem[i].pos)
			if d < max_distance_so_far {
				max_distance_so_far = d
				found = value.mem[i]
			}
		}
		return found, max_distance_so_far < f32(100000)
	}
	panic(fmt.tprint("not reached: ", loc))
}

find_containing_bucket :: proc(using tree: Tree($T, $N), pos: Position) -> ^Bucket(T, N) {
	switch value in tree.value {
	case Subdivision(T):
		subdivision := value
		if pos[dir] < subdivision.at {
			return find_containing_bucket(subdivision.sides[0]^, pos)
		} else {
			return find_containing_bucket(subdivision.sides[1]^, pos)
		}
	case ^Bucket(T):
		return value
	}
	panic(fmt.tprint("not reached: ", #procedure))
}


_distance :: proc(d1, d2: [2]f32) -> f32 {
	return math.sqrt((d1.x - d2.x) * (d1.x - d2.x) + (d1.y - d2.y) * (d1.y - d2.y))
}

traverse :: proc(
	tree: Tree($T, $N),
	data: rawptr,
	s: proc(_: Subdivision(T), _: rawptr),
	b: proc(_: ^Bucket(T, N), _: rawptr),
) {
	switch value in tree.value {
	case Subdivision(T):
		s(value, data)
		traverse(value.sides[0]^, data, s, b)
		traverse(value.sides[1]^, data, s, b)
	case ^Bucket(T):
		b(value, data)
	}
}
