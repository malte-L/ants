package main


import "core:fmt"
import "core:math"
import rl "vendor:raylib"


main :: proc() {


	fmt.println("hello world")


	rl.InitWindow(100, 100, "dot")
	rl.SetWindowState(rl.ConfigFlags{.WINDOW_RESIZABLE})
	rl.SetTargetFPS(60)


	for !rl.WindowShouldClose() {

		rl.BeginDrawing()
		{
			rl.DrawLineV({50, 50}, {100, 50}, rl.RED)
			rl.DrawLineV({50, 50}, rl.GetMousePosition(), rl.RED)
			fmt.println(
				0 <
				dot(
					normalize({100, 50} - {50, 50}),
					normalize(V2(rl.GetMousePosition()) - {50, 50}),
				),
			)
			rl.ClearBackground(rl.BLACK)
		}
		rl.EndDrawing()


	}


	V2 :: [2]f32
	dot :: proc(v1, v2: V2) -> f32 {
		t := v1 * v2
		return t.x + t.y
	}

	normalize :: proc(v: V2) -> V2 {
		length := math.sqrt(v.x * v.x + v.y * v.y)
		if length == 0 {
			return v
		}
		return v / length
	}

}
